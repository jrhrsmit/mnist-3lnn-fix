/**
 * @file main.c
 *
 * @mainpage MNIST 3-Layer Neural Network
 *
 * @brief Simple feed-forward neural network with 3 layers of nodes (input,
 * hidden, output) using Sigmoid or Tanh activation function and back
 * propagation to classify MNIST handwritten digit images.
 *
 * @details Simple feed-forward neural network with 3 layers of nodes (input,
 * hidden, output) using Sigmoid or Tanh activation function and back
 * propagation to classify MNIST handwritten digit images.
 *
 * @see [Simple 3-Layer Neural Network for MNIST Handwriting
 * Recognition](http://mmlind.github.io/Simple_3-Layer_Neural_Network_for_MNIST_Handwriting_Recognition/)
 * @see http://yann.lecun.com/exdb/mnist/
 * @version [Github Project Page](http://github.com/mmlind/mnist-3lnn/)
 * @author [Matt Lind](http://mmlind.github.io)
 * @date August 2015
 *
 */

#include <stdlib.h>
#include <time.h>

#include "3lnn.h"
#include "mnist-stats.h"
#include "mnist-utils.h"
#include "mnist/mnist_data.h"
#include "screen.h"


/**
 * @brief Training the network by processing the MNIST training set and updating
 * the weights
 * @param nn A pointer to the NN
 */

void trainNetwork(Network* nn) {
    int errCount = 0;

    // Loop through all images in the file
    for (int imgCount = 0; imgCount < num_train_imgs; imgCount++) {
        // Reading next image and its corresponding label
        MNIST_Image img = getImage(1, imgCount);
        MNIST_Label lbl = getLabel(1, imgCount);

        // Convert the MNIST image to a standardized vector format and feed into
        // the network
        feedInput(nn, img);

        // Feed forward all layers (from input to hidden to output) calculating
        // all nodes' output
        feedForwardNetwork(nn);

        // Back propagate the error and adjust weights in all layers accordingly
        backPropagateNetwork(nn, lbl);

        // Classify image by choosing output cell with highest output
        int classification = getNetworkClassification(nn);
        if (classification != lbl)
            errCount++;

#ifndef PRINT
        // Display progress during training
        displayTrainingProgress(imgCount, errCount, 3, 5);
        displayImage(img, lbl, classification, 7, 6);
#endif
    }
}

/**
 * @brief Testing the trained network by processing the MNIST testing set
 * WITHOUT updating weights
 * @param nn A pointer to the NN
 */

int testNetwork(Network* nn) {
    int errCount = 0;

    // Loop through all images in the file
    for (int imgCount = 0; imgCount < num_test_imgs; imgCount++) {
        // Reading next image and its corresponding label
        MNIST_Image img = getImage(0, imgCount);
        MNIST_Label lbl = getLabel(0, imgCount);

        // Convert the MNIST image to a standardized vector format and feed into
        // the network
        feedInput(nn, img);

        // Feed forward all layers (from input to hidden to output) calculating
        // all nodes' output
        feedForwardNetwork(nn);

        // Classify image by choosing output cell with highest output
        int classification = getNetworkClassification(nn);
        if (classification != lbl)
            errCount++;
#ifndef PRINT
        // Display progress during testing
        displayTestingProgress(imgCount, errCount, 5, 5);
        displayImage(img, lbl, classification, 7, 6);
#endif
    }
    return 100 - (errCount * 100 / num_test_imgs);
}

/**
 * @details Main function to run MNIST-1LNN
 */

int main(void) {
    // remember the time in order to calculate processing time at the end
    time_t startTime = time(NULL);

    // clear screen of terminal window
#ifndef PRINT
    clearScreen();
    printf(
        "    MNIST-3LNN: a simple 3-layer neural network processing the MNIST "
        "handwritten digit images\n\n");
#endif

    // Create neural network using a manually allocated memory space
    Network* nn = createNetwork(
        (MNIST_IMG_HEIGHT / yscale) * (MNIST_IMG_WIDTH / xscale), 20, 10);

    //    displayNetworkWeightsForDebugging(nn);
    //    exit(1);

    // Training the network by adjusting the weights based on error using the
    // TRAINING dataset
    for (int epoch = 0; epoch < NEPOCH; epoch++)
        trainNetwork(nn);

#ifdef PRINT
    printNetwork(nn);
    //printSigmoidLut();
#endif
    // Testing the during training derived network using the TESTING dataset

    // Free the manually allocated memory for this network
    free(nn);

#ifndef PRINT
    locateCursor(36, 5);

    // Calculate and print the program's total execution time
    time_t endTime = time(NULL);
    double executionTime = difftime(endTime, startTime);
    printf("\n    DONE! Total execution time: %.1f sec\n\n", executionTime);
    printf("Score: %d%%\n", testNetwork(nn));
#endif

    return 0;
}
