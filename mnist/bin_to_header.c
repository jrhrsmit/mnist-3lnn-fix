#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_help(void) {
    printf(
        "usage: bin_to_header [xscale] [yscale] [ntrain images] [ntest "
        "images]\n");
}

void write_hdr_start(FILE* f) {
    fprintf(f, "#ifndef MNIST_DATA_H_\n#define MNIST_DATA_H_\n\n");
    fprintf(f, "#include <stdint.h>\n\n");
}

void write_hdr_end(FILE* f) {
    fprintf(f, "#endif //  MNIST_DATA_H_\n\n");
}

#define NUMBER_TRAIN_IMAGES 60000
#define NUMBER_TEST_IMAGES 10000
#define MNIST_IMG_WIDTH 28
#define MNIST_IMG_HEIGHT 28
#define NUMBER_IMAGES (NUMBER_TEST_IMAGES + NUMBER_TRAIN_IMAGES)

int main(int argc, char** argv) {
    int i, j;
    uint32_t tmp;
    char fn_test_labels[] = "t10k-labels-idx1-ubyte";
    char fn_train_labels[] = "train-labels-idx1-ubyte";
    char fn_test_images[] = "t10k-images-idx3-ubyte";
    char fn_train_images[] = "train-images-idx3-ubyte";
    char filename_output[] = "mnist_data.h";
    const size_t offset_labels = 8;
    const size_t offset_images = 16;
    uint8_t* buf;
    FILE *fin, *fout;
    int xscale, yscale, ntest_img, ntrain_img;
    int x, y, xi, yi;
    uint32_t pixval;
    uint8_t* imgvec_in;
    uint32_t* imgvec_out;
    uint32_t mnist_data_width;
    uint32_t mnist_npix;
    uint32_t mnist_img_ndata;
    if (argc == 5) {
        if (sscanf(argv[1], "%d", &xscale) == 1 &&
            sscanf(argv[2], "%d", &yscale) == 1 &&
            sscanf(argv[3], "%d", &ntrain_img) == 1 &&
            sscanf(argv[4], "%d", &ntest_img) == 1) {
            if (xscale < 1 || xscale > 4 || yscale < 1 || yscale > 4) {
                fprintf(stderr, "xscale and yscale must be between 1 and 4\n");
                return 1;
            }
            if (ntest_img < 1 || ntest_img > NUMBER_TEST_IMAGES)
                ntest_img = NUMBER_TEST_IMAGES;
            if (ntrain_img < 1 || ntrain_img > NUMBER_TRAIN_IMAGES)
                ntrain_img = NUMBER_TRAIN_IMAGES;
        } else {
            print_help();
            return 1;
        }
    } else {
        print_help();
        return 1;
    }
    /* set mnist constants */
    mnist_data_width = 32;
    mnist_npix = (28 / xscale) * (28 / yscale);
    mnist_img_ndata =
        (((28 / xscale) * (28 / yscale)) + (mnist_data_width - 1)) /
        mnist_data_width;
    /* create file and start of file */
    fout = fopen(filename_output, "w+");
    write_hdr_start(fout);

    /* write parameters */
    fprintf(fout, "typedef uint%d_t* MNIST_Image;\n", mnist_data_width);
    fprintf(fout, "static const uint16_t xscale = %d;\n", xscale);
    fprintf(fout, "static const uint16_t yscale = %d;\n", yscale);
    fprintf(fout, "static const uint16_t mnist_data_width = %d;\n", mnist_data_width);
    fprintf(fout, "static const uint16_t num_train_imgs = %d;\n", ntrain_img);
    fprintf(fout, "static const uint16_t num_test_imgs = %d;\n\n", ntest_img);

    /* train labels */
    /* allocate space */
    buf = (uint8_t*)calloc(ntrain_img, sizeof(uint8_t));
    /* load data */
    fin = fopen(fn_train_labels, "rb");
    fseek(fin, offset_labels, SEEK_SET);
    fread(buf, 1, ntrain_img, fin);
    fclose(fin);
    /* write formatted data ot out file */
    fprintf(fout, "static const uint8_t lbl_train[%d] = {\n\t\t", (ntrain_img + 1) / 2);
    for (i = 0; i < ntrain_img; i++) {
        tmp = buf[i];
        i++;
        if (i < ntrain_img)
            tmp |= buf[i] << 4;
        fprintf(fout, "0x%02X%s", tmp, i < ntrain_img - 1 ? ", " : "");
        if ((i + 1) % 16 == 0)
            fprintf(fout, "\n\t\t");
    }
    fprintf(fout, "};\n\n");
    free(buf);

    /* test labels */
    /* allocate space */
    buf = (uint8_t*)calloc(ntest_img, sizeof(uint8_t));
    /* load data */
    fin = fopen(fn_test_labels, "rb");
    fseek(fin, offset_labels, SEEK_SET);
    fread(buf, 1, ntest_img, fin);
    fclose(fin);
    /* write formatted data ot out file */
    fprintf(fout, "static const uint8_t lbl_test[%d] = {\n\t\t", (ntest_img + 1) / 2);
    for (i = 0; i < ntest_img; i++) {
        tmp = buf[i];
        i++;
        if (i < ntest_img)
            tmp |= buf[i] << 4;
        fprintf(fout, "0x%02X%s", tmp, i < ntest_img - 1 ? ", " : "");
        if ((i + 1) % 16 == 0)
            fprintf(fout, "\n\t\t");
    }
    fprintf(fout, "};\n\n");
    free(buf);

    /* train images */
    /* allocate space */
    buf = (uint8_t*)calloc(ntrain_img * 28 * 28, sizeof(uint8_t));
    if (buf == NULL) {
        fprintf(stderr, "Could not allocate %d bytes of memory.\n",
                ntrain_img * 28 * 28);
        return 1;
    }
    /* load data */
    fin = fopen(fn_train_images, "rb");
    if (fin == NULL) {
        fprintf(stderr, "Could not open file \"%s\".\n", fn_train_images);
        fclose(fin);
        return 1;
    }
    if (fseek(fin, offset_images, SEEK_SET)) {
        fprintf(stderr, "Could not seek in file \"%s\".\n", fn_train_images);
        fclose(fin);
        return 1;
    }
    if (fread(buf, 1, ntrain_img * 28 * 28, fin) != ntrain_img * 28 * 28) {
        fprintf(stderr, "Could not read %d bytes from file \"%s\".\n",
                ntrain_img * 28 * 28, fn_train_images);
        fclose(fin);
        return 1;
    }
    fclose(fin);

    fprintf(fout, "static const uint%d_t img_train[%d][%d] = {\n\t\t", mnist_data_width,
            ntrain_img, mnist_img_ndata);
    imgvec_out = (uint32_t*)calloc(mnist_img_ndata, (mnist_data_width + 1) / 8);
    for (j = 0; j < ntrain_img; j++) {
        imgvec_in = &buf[j * 28 * 28];
        memset(imgvec_out, 0, mnist_img_ndata * ((mnist_data_width + 1) / 8));
        //printf("\nimg %d\n", j);
        for (i = 0; i < mnist_npix; i++) {
            pixval = 0;
            for (x = 0; x < xscale; x++) {
                xi = (i % (28 / xscale)) * xscale + x;
                for (y = 0; y < yscale; y++) {
                    yi = (i / (28 / yscale)) * yscale + y;
                    pixval += imgvec_in[yi * MNIST_IMG_WIDTH + xi];
                }
            }
            pixval = pixval > (xscale * yscale * 127) ? 1 : 0;
            imgvec_out[i / mnist_data_width] |= pixval
                                                << (i % mnist_data_width);
            //printf("%s%s", pixval > 0 ? "X" : ".",
            //       (i + 1) % (28 / xscale) == 0 ? "\n" : "");
        }
        fprintf(fout, "{");
        for (i = 0; i < mnist_img_ndata; i++) {
            fprintf(fout, "0x%0*X%s", (mnist_data_width + 3) / 4, imgvec_out[i],
                    i < mnist_img_ndata - 1 ? ", " : "");
        }
        fprintf(fout, "%s", j < ntrain_img - 1 ? "},\n\t\t" : "}\n\t");
    }
    fprintf(fout, "};\n\n");
    free(imgvec_out);
    free(buf);

    /* test images */
    /* allocate space */
    buf = (uint8_t*)calloc(ntest_img * 28 * 28, sizeof(uint8_t));
    if (buf == NULL) {
        fprintf(stderr, "Could not allocate %d bytes of memory.\n",
                ntest_img * 28 * 28);
        return 1;
    }
    /* load data */
    fin = fopen(fn_test_images, "rb");
    if (fin == NULL) {
        fprintf(stderr, "Could not open file \"%s\".\n", fn_test_images);
        fclose(fin);
        return 1;
    }
    if (fseek(fin, offset_images, SEEK_SET)) {
        fprintf(stderr, "Could not seek in file \"%s\".\n", fn_test_images);
        fclose(fin);
        return 1;
    }
    if (fread(buf, 1, ntest_img * 28 * 28, fin) != ntest_img * 28 * 28) {
        fprintf(stderr, "Could not read %d bytes from file \"%s\".\n",
                ntest_img * 28 * 28, fn_test_images);
        fclose(fin);
        return 1;
    }
    fclose(fin);

    fprintf(fout, "static const uint%d_t img_test[%d][%d] = {\n\t\t", mnist_data_width,
            ntest_img, mnist_img_ndata);
    imgvec_out = (uint32_t*)calloc(mnist_img_ndata, (mnist_data_width + 1) / 8);
    for (j = 0; j < ntest_img; j++) {
        imgvec_in = &buf[j * 28 * 28];
        memset(imgvec_out, 0, mnist_img_ndata * ((mnist_data_width + 1) / 8));
        //printf("\nimg %d\n", j);
        for (i = 0; i < mnist_npix; i++) {
            pixval = 0;
            for (x = 0; x < xscale; x++) {
                xi = (i % (28 / xscale)) * xscale + x;
                for (y = 0; y < yscale; y++) {
                    yi = (i / (28 / yscale)) * yscale + y;
                    pixval += imgvec_in[yi * MNIST_IMG_WIDTH + xi];
                }
            }
            pixval = pixval > (xscale * yscale * 127) ? 1 : 0;
            imgvec_out[i / mnist_data_width] |= pixval
                                                << (i % mnist_data_width);
           // printf("%s%s", pixval > 0 ? "X" : ".",
           //        (i + 1) % (28 / xscale) == 0 ? "\n" : "");
        }
        fprintf(fout, "{");
        for (i = 0; i < mnist_img_ndata; i++) {
            fprintf(fout, "0x%0*X%s", (mnist_data_width + 3) / 4, imgvec_out[i],
                    i < mnist_img_ndata - 1 ? ", " : "");
        }
        fprintf(fout, "%s", j < ntest_img - 1 ? "},\n\t\t" : "}\n\t");
    }
    fprintf(fout, "};\n\n");
    free(imgvec_out);
    free(buf);
    
    write_hdr_end(fout);
    fclose(fout);
    return 0;
}
