#ifndef Q_H_
#define Q_H_

#define SATURATION

#define FLOAT_CONVERSIONS

#include <stdint.h>

// Qm.f
#define M 4
#define F 12

#define SAT_MIN ((q_t)(1 << (F + M - 1)))
#define SAT_MAX ((q_t)0x7FFF)

typedef int16_t q_t;
typedef int32_t q_long_t;

//#define M 20
//#define F 12
//#define SAT_MIN ((q_t)(1 << (F + M - 1)))
//#define SAT_MAX ((q_t)0x7FFFFFFF)
//typedef int32_t q_t;
//typedef int64_t q_long_t;

#define q_one (1 << F)

q_t q_add(q_t a, q_t b);
q_t q_sub(q_t a, q_t b);
q_t q_mul(q_t a, q_t b);
q_t q_div(q_t a, q_t b);
q_t q_sigmoid(q_t a);

#ifdef FLOAT_CONVERSIONS
q_t fq(double f);
double qf(q_t a);
#endif

#endif // Q_H_
