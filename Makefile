IDIR=inc
ODIR=obj

NEPOCH ?= 3

SRCDIR=src

CFLAGS = -I$(IDIR) -I.
CFLAGS += -Wall -Wextra -lm
#CFLAGS += -Wno-unused-function

ifneq (,$(PRINT))
CFLAGS += -DPRINT
endif

ifneq (,${GPROF})
CFLAGS += -pg
endif
ifneq (,${DEBUG})
CFLAGS += -O0 -DDEBUG -fstack-protector-all -ggdb -g
else
CFLAGS += -O3
endif

TARGET=mnist-3lnn
CC = gcc
LDFLAGS = -L.. #-s -Wl,-Map,mnist_3lnn.map
CFLAGS += -I/usr/include
CFLAGS += --std=c99
CFLAGS += -DNEPOCH=$(NEPOCH)

_SRCS=$(foreach dir, $(SRCDIR), $(wildcard $(dir)/*.c))
_SRCDIRS=$(SRCDIR) 

_OBJ = $(addprefix $(ODIR)/,$(notdir $(_SRCS)))
OBJ = $(_OBJ:%.c=%.o)

${TARGET}: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

$(ODIR):
	mkdir -p $(ODIR)

$(ODIR)/%.o: $(SRCDIR)/%.c $(ODIR) mnist/mnist_data.h
	mkdir obj -p
	$(CC) -c -o $@ $< $(CFLAGS)

mnist/mnist_data.h:
	make -C mnist

.PHONY: clean test-targets

test-targets:
	@echo SRCDIRS
	@echo $(_SRCDIRS)
	@echo SRCDIR
	@echo $(SRCDIR)
	@echo LIBDIR
	@echo $(LIBDIR)
	@echo Sources:
	@echo $(_SRCS)
	@echo Objects:
	@echo $(OBJ)

clean:
	rm -f $(ODIR)/*.o  $(TARGET)
	make -C mnist clean
